package Matrice;


public class MatrixMethods {
    /**
     * The method prints the whole matrix
     * @param matrix forwards the matrix to the method
     */
    public void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                    System.out.print("   m[" + i + "][" + j + "]: " + matrix[i][j]);

            }
            System.out.println();
        }

    }
    /**
     * This method is for finding the average value of a row that we passed to the method
     * @param matrix forwards the matrix to the method
     * @param row the number of a row
     * @return returns average(double)
     */
    public double rowSum(int[][] matrix, int row) {
        int max = 0;
        double average = 0;
        int counter = 0;

        for (int j = 0; j < matrix[row].length; j++) {
            max += matrix[row][j];
            counter++;
        }
        average = (double) max / counter;


        return  average;
    }

    /**
     * This method is for finding side diagonal from matrix
     * @param matrix forwards the matrix to the method
     * @return returns int array
     */

    public int[] diagonal(int[][] matrix) {
        int[] arrey1 = new int[matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                if (j + i == matrix.length - 1) {   // side diagonal elements are elements on position i+j=matrix size-1
                    arrey1[i] = matrix[i][j];

                }
            }
        }

        return arrey1;
    }

    /**
     * Method for finding and printing elements above and on side diagonal
     * @param matrix forwards the matrix to the method
     */

    public void aboveDiagonal(int[][] matrix) {
        System.out.println();
        int[][] array = new int[matrix.length][matrix.length];
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[i].length;j++){
                if(i+j < matrix.length-1){ // elements above and ond side diagonal have sum of indexes =matrix size or <matrix size
                    System.out.print("    m["+i+"]["+j+"]:"+matrix[i][j]);
                }

            }
            System.out.println();
        }
    }

}
