package Matrice;

public class Main {

    public static void main(String[] args) {
        int[][] matrix = new int[4][4];     // initialized matrix
        int counter = 0;    // counter for filling matrix with increasing integer values
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                counter++;
                matrix[i][j] = counter;
            }
        }

        MatrixMethods method = new MatrixMethods();     // creating an object from class MatrixMethods
        method.printMatrix(matrix);     // calling a method to print the matrix
        System.out.println("Average value of row that u wanted is : " + method.rowSum(matrix, 2));  //printing average value of a row throw rowSum method
        int niz[] = method.diagonal(matrix);    // putting diagonal elements from matrix to array using method diagonal
        for (int i = 0; i < niz.length; i++) {
            System.out.print(niz[i] + "   ");       // printing that array
        }
        method.aboveDiagonal(matrix);       //method for getting all the elements on and above side diagonal

    }

}
