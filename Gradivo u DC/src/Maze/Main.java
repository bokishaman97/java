package Maze;


public class Main {

    public static void main(String[] args) {

        int[][] myMaze = {
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0, 0, 0, 0},
                {3, 1, 1, 0, 1, 0, 0, 1, 1, 1},
                {0, 1, 0, 0, 1, 0, 0, 1, 0, 0},
                {0, 1, 1, 1, 1, 0, 1, 1, 1, 0},
                {0, 1, 0, 0, 1, 0, 1, 0, 1, 0},
                {0, 0, 0, 0, 1, 1, 1, 0, 1, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 1, 2},
                {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},

        };


        PathToFinish obj = new PathToFinish();
        obj.shortestPath(myMaze);


    }
}
