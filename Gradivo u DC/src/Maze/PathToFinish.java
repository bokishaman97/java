package Maze;

import java.util.LinkedList;

public class PathToFinish {

    public PathToFinish() {
    }


    /**
     * Shortest Path throw matrix maze
     * @param matrix matrix which is our maze
     */
    public void shortestPath(int[][] matrix) {

        Field[][] fild = new Field[matrix.length][matrix[0].length];// Creating matrix of a class Model and passing the length of maze
        Field src = getStart(matrix);


        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] != 0) {    // see if the field is a wall
                    if (matrix[i][j] == 3) {    // see if the field is finish
                        fild[i][j] = new Field(i, j, Integer.MAX_VALUE, null, 3);   // if it is finish make and object of it s parameters
                    } else
                        fild[i][j] = new Field(i, j, Integer.MAX_VALUE, null, 1);    //if it s not 0;3 then store that field as a path to finish
                }
            }
        }

        LinkedList<Field> queue = new LinkedList<>();    // empty queue


        src.setDist(0);     // setting the beginning distance to 0
        queue.add(src);     // putting the source coordinates
        Field finish = null;  // finishing field
        Field p;    // current field

        while ((p = queue.poll()) != null) {
            if (p.getType() == 3) {                    // asking if the current field is finish
                finish = p;
                break;
            }
            // moving up
            visited(fild, queue, p.getX() - 1, p.getY(), p);
            //moving down
            visited(fild, queue, p.getX() + 1, p.getY(), p);
            //moving left
            visited(fild, queue, p.getX(), p.getY() - 1, p);
            // moving right
            visited(fild, queue, p.getX(), p.getY() + 1, p);

        }

        // destination not reached
        isDestinationReached(finish, p);

    }

    /**
     * Finding the starting coordinates
     * @param matrix our maze
     * @return  Field with coordinates of the finish
     */
    private Field getStart(int[][] matrix) {
        Field src = new Field();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                if (i == 0 || i==matrix.length-1 ) {
                    if (matrix[i][j] == 2) {


                        src.setX(i);   // beginning coordinates
                        src.setY(j);   // beginning coordinates
                    }
                }

                if (j == 0 || j==matrix.length-1) {
                    if (matrix[i][j] == 2) {


                        src.setX(i);   // beginning coordinates
                        src.setY(j);   // beginning coordinates
                    }
                }

            }
        }
        return src;
    }


    /**
     * Method for testing where that path is and witch fields were visited6
     * @param fields matrix the the all coordinates of path and finishing field
     * @param queue  used to save the parent and distance , also fields with coordinates are saved in it
     * @param x      coordinates i (index)
     * @param y      coordinates j (index)
     * @param parent current field
     */
    private void visited(Field[][] fields, LinkedList<Field> queue, int x, int y, Field parent) {
        if (x < 0 || x >= fields.length || y < 0 || y >= fields[0].length || fields[x][y] == null) {
            /*
            x < 0 & y < 0 if we are out of bound
            x >= field.length & y >=field[0].length - the length is 10, it can t be 10 nor grater
            fields[x][y] == null - if the passed coordinates are null
             */
            return;     //return if only one of this statements are true
        }
        int distance = parent.getDist() + 1;    // increment distance
        Field p = fields[x][y];     //temp of field that is passed to the method

        if (distance < p.getDist()) {   // finding the shortest distance
            p.setDist(distance);    // saving distance
            p.setPrev(parent);   // saving parent into previous field
            queue.add(p);   // adding into queue
        }
    }

    /**
     * Method for printing path
     * @param path list with coordinates
     */
    private void printPath(LinkedList<Field> path) {
        for (Field i : path) {
            System.out.print(i.getX() + " " + i.getY() + ",");  // printing the coordinates(path) from start to finish
        }

    }

    /**
     * Finding the right destination and printing it
     * @param finish finishing field
     * @param p      model of the field that contain coordinates of the path to finish
     */
    private void isDestinationReached(Field finish, Field p) {
        if (finish == null) {
            return;
        } else {

            LinkedList<Field> path = new LinkedList<>();    // empty linked list which saves the path to finish
            do {
                path.addFirst(p);   // add current p to the list

            } while ((p = p.getPrev()) != null);    // moving in reverse , from finish to start

            printPath(path);
        }
    }
}




