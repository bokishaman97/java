package Maze;

/**
 * On this class I implemented encapsulation
 *
 */
public class Field {
    private int x;
    private int y;
    private int dist;
    private Field prev;
    private int type;
    public Field(){

    }
    /**
     * Constructor
     * @param x coordinates of i (index)
     * @param y coordinates of j (index)
     * @param dist distance to the finish
     * @param prev previous element of current field
     * @param type type stores 1 or 3 (1-path ; 3-finish)
     */
    public Field(int x, int y, int dist, Field prev, int type) {
        this.x = x;
        this.y = y;
        this.dist = dist;
        this.prev = prev;
        this.type = type;

    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public Field getPrev() {
        return prev;
    }

    public void setPrev(Field prev) {
        this.prev = prev;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
