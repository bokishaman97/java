package Minmax;

public class MinMaxSum {
    /**Method for finding sum without max and sum without min
     * @param array forwarding an array to method
     * @return int array
     */
    public int[] minMaxSum(int[] array) {
        int min = array[0];
        int max = 0;
        int[] finish = new int[2];
        for (int i = 0; i < array.length; i++) {
            //finding max in array
            if (array[i] > max) {
                max = array[i];

            }
            // finding min in array
            if (array[i] < min) {
                min = array[i];
            }
        }

        finish[0] = sumMin(array, min); // calling method sumMin to get a value for sum without min
        finish[1] = sumMax(array, max, min);    //calling method sumMax to get a value for sum without max
        return finish;
    }

    /**Method for getting sum of a array without min
     * @param array forwarding an array to method
     * @param num  forwarding min element
     * @return  int
     */
    public int sumMin(int[] array, int num) {

        int minSum = 0;
        for (int j = 0; j < array.length; j++) {
            if (array[j] == num) {
                array[j] = 0;
            }
            minSum += array[j];
        }

        return minSum;

    }

    /**Method for getting sum of a array without max
     * @param array forwarding an array to method
     * @param num max value
     * @param num1 min value
     * @return int
     */
    public int sumMax(int[] array, int num, int num1) {

        int maxSum = num1;
        for (int j = 0; j < array.length; j++) {
            if (array[j] == num) {
                array[j] = 0;
            }
            maxSum += array[j];
        }


        return maxSum;
    }
}

