package Minmax;

public class Main {

    public static void main(String[] args) {
        int arrey[] = {4,8,2,5,7};  // declared an array
        MinMaxSum obj = new MinMaxSum();    // object from class MinMazSum
        int[] finish = new int[2];  // array of 2 elements ,1 min,1 max
        finish = obj.minMaxSum(arrey);  // putting sum max,sum min in the array using method minMaxSum
        for (int i = 0; i < finish.length; i++) {
            System.out.println("The answer for this task is: " + finish[i]);
        }
    }
}
