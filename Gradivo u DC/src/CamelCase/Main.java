package CamelCase;

public class Main {
    public static void main(String[] args) {
        String string = "saveChangesInTheEditor";   // string for testing
        CamelCase obj = new CamelCase();    // creating and object for class CamelCase
        int finish = obj.camelCase(string); // variable finish get the value of number of words from method camelCase
        System.out.println("Num. of words:" + finish);      //print how many words there is
    }

}
