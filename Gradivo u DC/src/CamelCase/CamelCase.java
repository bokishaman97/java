package CamelCase;


public class CamelCase {
    /**
     * Method for getting the number of the words written in CamelCase notation
     * @param word forwarding String to the method
     * @return int , number of words in the String
     */
    public int camelCase(String word) {
        int beginning = 0;   // beginning of the string
        int finish = word.length() - 1; // end of the string
        int counter = 1;    // counter for every word in string
        while (beginning <= finish) {
            if (!isUpperCase(word.charAt(beginning))) {  //calling method isUpperCase to find upper letters
                beginning++;    //increment the beginning if the letter isn t upper
            } else {
                beginning++;    // if the letter is upper increment beginning and counter
                counter++;
            }
        }
        return counter;
    }

    /**
     * Method for asking if the character is upper
     * @param ch character for testing
     * @return boolean
     */
    public boolean isUpperCase(char ch) {
        if (ch > 'A' && ch < 'Z') { //asking if the letter is between 'A'-'Z'
            return true;
        } else return false;

    }
}
