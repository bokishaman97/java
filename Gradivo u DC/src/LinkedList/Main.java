package LinkedList;


public class Main {
    public static void main(String[] args) {
       SortedLinkedList list1 = new SortedLinkedList();


        list1.add(3);
        list1.printList();
        System.out.println();
        list1.add(4);
        list1.printList();
        System.out.println();
        list1.add(1);
        list1.printList();
        System.out.println();
        list1.add(-2);
        list1.printList();
        System.out.println();
        list1.add(2);
        list1.printList();
        System.out.println();
        list1.add(67);
        list1.printList();
        System.out.println();
        list1.add(5);
        list1.printList();
        System.out.println();
    }

}
