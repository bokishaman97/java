package LinkedList;

public class SortedLinkedList extends LinkedList {
    public SortedLinkedList() {

    }

    @Override
    public void add(double data) {
        Node object = new Node(data);

        if (head == null) {
            head = object;
            currentSize++;
        } else {
            Node temp = head;
            while (temp.getNext() != null) {
                if (object.getData() < temp.getData()) {
                    addFirst(data);
                    currentSize++;
                    return;
                }else if(object.getData()<temp.getNext().getData()){
                        object.setNext(temp.getNext());
                        temp.setNext(object);
                        return;
                }
                temp=temp.getNext();
            }
            object.setNext(temp.getNext());
            temp.setNext(object);
            currentSize++;
        }
    }
}
