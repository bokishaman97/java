package LinkedList;


public class LinkedList {
    Node head = null;
    public int currentSize = 0;

    public void add(double data) {
        Node object = new Node(data);

        if (head == null) {
            head = object;
            currentSize++;
        } else {
            Node temp = head;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(object);
            currentSize++;
        }
    }

        public void addFirst(double data){
        Node object = new Node(data);
        Node temp = head;
        object.setNext(temp);
        head=object;
        currentSize++;
    }
    public void removeFromPosition(int index) {
        if (index > currentSize) {
            System.out.println("Index out of bound");
            return;
        }
        // If linked list is empty
        if (head == null)
            return;

        // Store head node
        Node temp = head;

        // If head needs to be removed
        if (index == 0) {
            head = temp.getNext();   // Change head
            return;
        }

        // Find previous node of the node to be deleted
        for (int i = 0; temp != null && i < index - 1; i++)
            temp = temp.getNext();

        // If position is more than number of ndoes
        if (temp == null || temp.getNext() == null)
            return;

        // Node temp->next is the node to be deleted
        // Store pointer to the next of node to be deleted
        Node next = temp.getNext().getNext();

        temp.setNext(next);  // Unlink the deleted node from list
        currentSize--;
    }

    public void printList() {

        Node temp = head;
        if (temp == null) {
            System.out.println("List is empty");
        }
        System.out.print("( ");
        while (temp != null) {
            System.out.print(temp.getData() + "   ");
            temp = temp.getNext();
        }
        System.out.print(" )");
    }

    public void addAtPosition(int index, double data) {
        Node object = new Node(data);
        if (index == 0) {
            object.setNext(head);
            head = object;
            currentSize++;
        } else {
            Node temp = head;
            while (--index > 0) {
                temp = temp.getNext();
            }
            object.setNext(temp.getNext());
            temp.setNext(object);
            currentSize++;
        }
    }

    public double averageValue() {
        Node temp = head;
        int max = 0;
        for (int i = 0; i < currentSize; i++) {
            max += temp.getData();
            temp = temp.getNext();
        }
        float average = (float) max / currentSize;

        return average;
    }

    public Node searchForTheNode(double data) {
        Node temp = head;

        for (int i = 1; i <= currentSize; i++) {
            if (temp.getData() == data) {
                System.out.println("node is found at " + i + " position");
                return temp;
            }
            temp = temp.getNext();
        }
        return null;
    }

    public void compareElements(int value1, int value) {
        Node first = searchForTheNode(value1);
        Node second = searchForTheNode(value);

        if (first == null || second == null)
            System.out.println("Node does not exist");
        else if (first.getData() > second.getData())
            System.out.println("First number is greater");
        else if (first.getData() < second.getData())
            System.out.println("Second number is greater");
        else if (first.getData() == second.getData())
            System.out.println("Elements are equal");
        else System.out.println("not found");
    }

    public void greaterThanGivenValue(int value) {
        Node temp = head;
        Node test = searchForTheNode(value);

        if (test == null) {
            System.out.println("Value does not exist");
            return;
        }

        for (int i = 0; i < currentSize; i++) {
            if (temp.getData() > value) {
                System.out.println(temp.getData() + " is greater than " + value);
            }
            temp = temp.getNext();
        }
    }

    // bubble sort
    public void sort() {
        boolean i;
        if(head==null){
            return;
        }

        do{
            Node temp1=head;
            i = false;

            while (temp1.getNext() != null) {
                if (temp1.getData() > temp1.getNext().getData()) {
                    swap(temp1, temp1.getNext());
                    i = true;
                }
                temp1 = temp1.getNext();
            }
        }while(i);
    }

    public void swap(Node p, Node q) {
        double temp;
        temp = p.getData();
        p.setData(q.getData());
        q.setData(temp);
    }
}


