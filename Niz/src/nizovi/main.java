package nizovi;

public class main {

	public static void main(String[] args) {
		int niz[] = {5, 3, 4, 2, 5, 6, 2, 8, 5};

		System.out.println("trazimo broj pod indexom 6");
		//prikaz broja na odredjenom indexu
		print a = new print();
		System.out.println("Num. on given index is  " + a.print(6, niz));
		//printovanje celog niza
		a.printAll(niz);
		// suma elemenata u nizu niz
		System.out.println("Sum of numbers is : " + a.suma(niz));


		//biz je prazan niz u kojem su pozitivni indexi popunjeni
		int biz[] = new int[niz.length];
		for (int i = 1; i <= niz.length; i++) {
			if (i % 2 == 0) {
				biz[i] = niz[i];
			}
		}
		a.printAll(biz);
		//elementi koji su veci od proseka niza
		int b[] = a.veciNegoProsek(niz);
		a.printAll(b);
		//unazad niz
		int unazad[] = a.unazadNiz(niz);
		a.printAll(unazad);
		//zamena svaka 2 elementa
		a.zamenaSvaka2Elementa(niz);
		// sortiran niz
		a.sort(niz);
	}

}
